<?php

namespace Drupal\canonical_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure canonical_config settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'canonical_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['canonical_config.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['origin'] = [
      '#type' => 'textfield',
      '#description' => 'Add the domain where do you want the canonical urls point. Add the domain without http:// or https:// if you want to use the active domain protocol, if you want to force it, add it',
      '#title' => $this->t('Domain for canonical metatag:'),
      '#default_value' => $this->config('canonical_config.settings')->get('origin'),
    ];

    $form['destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source domains to apply canonical metatag:'),
      '#description' => 'Add all needed domains separated by comma where do you want to add canonical metatag. Add the domain without http:// or https:// and final slash',
      '#default_value' => $this->config('canonical_config.settings')->get('destination'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
//    var_dump($form_state->getValue('origin'));
//    if ($form_state->getValue('example') != 'example') {
//      $form_state->setErrorByName('example', $this->t('The value is not correct.'));
//    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('canonical_config.settings')
      ->set('origin', $form_state->getValue('origin'))
      ->set('destination', $form_state->getValue('destination'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

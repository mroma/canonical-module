<?php

namespace Drupal\canonical_config\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for canonical_config routes.
 */
class CanonicalConfigController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
